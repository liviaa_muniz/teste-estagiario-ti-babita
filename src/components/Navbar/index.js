import "./stylenav.css"
import { Download, FileEarmarkTextFill, BoxArrowInRight } from "react-bootstrap-icons"
import icon from '../../assets/icon.png'
import { useHistory } from 'react-router-dom'

function Navbar() {
	const history=useHistory()
  	function navegarLogin(){
    	history.push("/")
    }
  return (
    <nav>
    	<div className="posicao">
	      <img src={icon}/>
	      <div className="item">
	      <Download color="white" size={24} />
	      <p className="letra">DOWNLOAD DE ARQUIVOS</p>
	      </div>
	      <div className="item">
	      <FileEarmarkTextFill color="white" size={24} />
	      <p className="letra">CONSULTAS</p>
	      </div>
      	</div>
	    <div className="logout" onClick={() => navegarLogin()}>
	    	<BoxArrowInRight color="white" size={24}/>
	    	<p>SAIR</p>
	    </div>
    </nav>
  );
}

export default Navbar;
