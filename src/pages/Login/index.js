import "./stylelogin.css"
import { PersonCircle, Lock, BoxArrowInRight } from "react-bootstrap-icons"
import { useHistory } from 'react-router-dom'
import icon from '../../assets/icon.png'

function Login() {
  const history=useHistory()
  function navegarHome(){
    history.push("/home")
  }
  return (
    <div className="fundo">
      <div>
      	<p className="titulo">THINK FORNECEDOR</p>
      	<div className="formulario">
          <div className="campos">
            <PersonCircle color="white" size={24} />
      		  <input placeholder="Usuário"></input>
          </div>
          <div className="campos">
            <Lock color="white" size={24} />
            <input placeholder="Senha"></input>
          </div>
      		<button onClick={navegarHome}>LOGIN <BoxArrowInRight color="#007bfd" size={24} /></button>
      	</div>
      </div>
      <img src={icon} className="logo"/>
    </div>
  );
}

export default Login;
